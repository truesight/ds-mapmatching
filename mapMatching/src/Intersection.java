
import java.util.LinkedList;


public class Intersection implements Comparable<Intersection> {
	final static double AVERAGE_RADIUS_OF_EARTH = 6371;
	long id;
	double lat;
	double lon;
	double fscore;
	double gscore;
	LinkedList<Intersection> connectedTo;

	public Intersection() {
		connectedTo = new LinkedList<Intersection>();
		fscore = 0;
		gscore = 0;
	}

	public Intersection(long num) {
		id = num;
		connectedTo = new LinkedList<Intersection>();
		fscore = 0;
		gscore = 0;
	}

	public long getId() {
		return id;
	}

	public void setLatLon(double latitude, double longditude) {
		lat = latitude;
		lon = longditude;
	}

	public double getLon() {
		return lon;
	}

	public double getLat() {
		return lat;
	}

	public void setGscore(double n) {
		gscore = n;
	}

	public double getGscore() {
		return gscore;
	}

	public void setFscore(double n) {
		gscore = n;
	}

	public double getFscore() {
		return gscore;
	}
	

	public LinkedList<Intersection> getConnections() {
		return connectedTo;
	}

	public boolean isConnected(Intersection v) {
		for(Intersection c : connectedTo) {
			if(v == c) {
				return true;
			}
		}
		return false;
	}

	public void connect(Intersection v) {
		if(!isConnected(v)) {
			connectedTo.add(v);
		}
	}

	public double distanceFrom(Intersection inters) {
		double lat2 = inters.getLat();
		double lon2 = inters.getLon();

		double latDistance = Math.toRadians(lat - lat2);
		double lngDistance = Math.toRadians(lon - lon2);

		double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2)
				+ Math.cos(Math.toRadians(lat)) * Math.cos(Math.toRadians(lat2))
				* Math.sin(lngDistance / 2) * Math.sin(lngDistance / 2);

		double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
		return (AVERAGE_RADIUS_OF_EARTH * c);
	}
	
	/*public double distanceFrom(Intersection inters) {		// Manhattan Distance
		double lat2 = inters.getLat();
		double lon2 = inters.getLon();

		double manhattanDist = Math.abs(lat-lat2) + Math.abs(lon-lon2);
		return manhattanDist;
	}*/

	public int compareTo(Intersection arg0) {
		if (fscore > arg0.getFscore()) {
			return -1;
		} else if (fscore < arg0.getFscore()) {
			return 1;
		} else {
			return 0;
		}
	}

}
