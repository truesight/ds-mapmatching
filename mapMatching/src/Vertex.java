

import java.util.LinkedList;

public class Vertex {
	private long label;
	private LinkedList<String> connectedTo;
	private LinkedList<Vertex> connectedToV;
	private LinkedList<Gps> gpsPoints;
	private Gps originalG;
	private Vertex predecessor;
	private double p1longitude; 
	private double distWithMe;
	private double p1latitude;
	private double p2longitude; 
	private double p2latitude;
	private boolean visited;
	
	public Vertex(long n) {
		this.label = n;
		gpsPoints = new LinkedList<Gps>();
		connectedTo = new LinkedList<String>();
		connectedToV = new LinkedList<Vertex>();
		distWithMe = 0;
		predecessor = null;
		visited = false;
	}
	
	public Gps getOriginalG() {
		return originalG;
	}

	public void setOriginalG(Gps originalG) {
		this.originalG = originalG;
	}

	public boolean isVisited() {
		return visited;
	}

	public void setVisited(boolean visited) {
		this.visited = visited;
	}

	public LinkedList<Gps> getGpsPoints() {
		return gpsPoints;
	}

	public void addGpsPoints(Gps g) {
		gpsPoints.add(g);
	}

	
	public void setDist(){
		double f = Math.sqrt(Math.pow(p2latitude-p1latitude,2) +Math.pow(p2longitude-p1longitude,2));
		distWithMe = f;
	}
	
	public double getDistWithMe() {
		return distWithMe;
	}
	public void addDistWithMe(double distWithMe) {
		this.distWithMe += distWithMe;
	}
	public void setP1longitude(double d) {
		this.p1longitude = d;
	}
	public void setP1latitude(double d) {
		this.p1latitude = d;
	}
	public void setP2longitude(double d) {
		this.p2longitude = d;
	}
	public void setP2latitude(double d) {
		this.p2latitude = d;
	}
	
	public double getp1Longitude() {
		return p1longitude;
	}
	
	public double getp1Latitude() {
		return p1latitude;
	}
	
	public double getp2Longitude() {
		return p2longitude;
	}
	
	public double getp2Latitude() {
		return p2latitude;
	}
	
	public long getLabel() {
		return label;
	}
	public LinkedList<String> getConnections() {
		return connectedTo;
	}
	
	public LinkedList<Vertex> getConnectionsV() {
		return connectedToV;
	}
	
	public void connect(String connected) {
		connectedTo.add(connected);
	}
	
	public void connect(Vertex connected) {
		connectedToV.add(connected);
	}
	
	public void seTconnect(LinkedList<Vertex> a) {
		connectedToV = a;
	}
	
	public Vertex getPredecessor() {
		return predecessor;
	}

	public void setPredecessor(Vertex predecessor) {
		this.predecessor = predecessor;
	}

	public LinkedList<Vertex> convert( LinkedList<Vertex> b){
		LinkedList<Vertex> tmp = new LinkedList<Vertex>();
		LinkedList<String> a = connectedTo;
		for(String i:a){
			for(Vertex j : b){
				if(i.charAt(0)==j.getLabel()){
					tmp.add(j);
				}
			}
		}
		return tmp;
	}



	
	
}
