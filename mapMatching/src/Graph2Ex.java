

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Queue;
import java.util.Scanner;
import java.util.Stack;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class Graph2Ex {
	static Map<Long, Intersection> intersections;
	static ArrayList<Intersection> intersectionList;
	static ArrayList<Vertex> edges;
	static long b = 0;
	static boolean running = true;
	static final float omegaR = 0.4F;
	static final float omegaT = 1-omegaR;

	//static int counter =0;
	static LinkedList<Gps> matchedGps = new LinkedList<Gps>();
	static LinkedList<Gps> gpses = new LinkedList<Gps>();
	static ArrayList<Vertex> deBugEdges = new ArrayList<Vertex>();
	public static void main(String[] args) throws Exception {
		Scanner reader = new Scanner(System.in);
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbf.newDocumentBuilder(); 
		//Document doc = db.parse(new File("highways.osm"));
		Document doc = db.parse(new File("highways2.osm"));

		intersections = new HashMap<Long, Intersection>();
		
		edges = new ArrayList<Vertex>();
		long a = System.nanoTime();	//time start
		System.out.println("start parsing map.osm");
		importIntersections(doc);
		b = System.nanoTime();
		System.out.println("Intersections imported, Time taken in ms: "+((b-a)/1000000));
		System.out.println("Begin constructing map");
		importRoads(doc);
		intersectionList = new ArrayList<Intersection>(intersections.values());
		System.out.println("Roads imported start connecting segments");
		System.out.println("But first let us debug");
		b = System.nanoTime();
		nodeDebug(intersectionList);
		System.out.println("nodes debugged, Total time taken in ms: "+((b-a)/1000000));

		nodeToEdge(edges);
		int countr=0;
		for(Vertex t: edges) {
			for(Vertex y: deBugEdges){
				if(t.getp1Latitude()==y.getp1Latitude() && t.getp1Longitude() == y.getp1Longitude() 
						&& t.getp2Latitude() == y.getp2Latitude()) {
					if(t.getConnectionsV().size()==y.getConnectionsV().size()) {
						//System.out.println(t.getConnectionsV().size()+" and "+ y.getConnectionsV().size());
						countr+=1;
					}

				}
			}
		}
		System.out.println(countr);
		System.out.println("Size vs "+edges.size()+" "+ deBugEdges.size());
		edges = deBugEdges;
		b = System.nanoTime();
		System.out.println("Graph done, Total time taken in ms: "+((b-a)/1000000));

		//The loop that determines how many times to run.
		while(running){
			System.out.println("Reading in the gps ccordinates");
			gpses.clear();
			try {
				List<String> lines = Files.readAllLines(Paths.get("gps.txt"),
						Charset.defaultCharset());
				for (String line : lines) {
					String[] tmp = line.split(" ");
					gpses.add(new Gps(Double.parseDouble(tmp[0]), Double.parseDouble(tmp[1])));
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
			System.out.println("Start matching");
			long c = System.nanoTime();
			bfs(edges.get(0),gpses.get(0));
			long d = System.nanoTime();
			Demo ndemo = new Demo(matchedGps,gpses);
			ndemo.setVisible(true);
			System.out.println("Matching done, Time in ms: "+((d-c)/1000000));
			boolean oige = false;
			boolean finished = false;
			while(!oige){
				System.out.println("Map again? (Y,N)");
				String yes=reader.next();
				if(yes.equals("Y") || yes.equals("y")){
					oige=true;
				}
				else if(yes.equals("N")|| yes.equals("n")){
					oige=true;
					finished=true;	//We are done
				}
				else{
					System.out.println("Wrong input, try again! ");
				}
			}
			if(finished){
				reader.close();
				break;
			}
		}
	}

	//import initial nodes
	public static void importIntersections(Document doc) throws IOException {

		NodeList nList = doc.getElementsByTagName("node");
		int len = nList.getLength();
		//System.out.println("for 1 parse "+nList.getLength());
		for (int i = 0; i < len; i++) {
			Node n = nList.item(i);
			Element e = (Element) n;
			long id = Long.valueOf(e.getAttribute("id"));
			Intersection inter = new Intersection(id);
			inter.setLatLon(Double.valueOf(e.getAttribute("lat")), Double.valueOf(e.getAttribute("lon")));
			intersections.put(id, inter);		
		}
	}
	//Connect links (road segments) to each other
	//Instead of this we need to change it so that in here the dots are connected later
	public static void nodeToEdge(ArrayList<Vertex> e){
		int length = edges.size();
		for(int k=0;k<length;k++){
			for(int l=0;l<length;l++){
				if(edges.get(k).getp2Latitude()== edges.get(l).getp1Latitude() &&
						edges.get(k).getp2Longitude()== edges.get(l).getp1Longitude() &&
						(edges.get(k).getp1Latitude()!= edges.get(l).getp2Latitude() &&
						edges.get(k).getp1Longitude()!= edges.get(l).getp2Longitude())){
					
					edges.get(k).connect(edges.get(l));
				}
			}
			edges.get(k).setDist();
		}

	}
	
	public static void nodeDebug(ArrayList<Intersection> intersectionL) {
		int len = intersectionL.size();
		
		for(int i=0;i<len-1;i++) {
			ArrayList<Vertex> tmp = new ArrayList<Vertex>();
			for(int j =0;j< intersectionL.get(i).getConnections().size();j++) {
				Vertex e = new Vertex(intersectionL.get(i).getId()+intersectionL.get(i).getConnections().get(j).getId());
				e.setP1latitude(intersectionL.get(i).getLat());
				e.setP1longitude(intersectionL.get(i).getLon());
				e.setP2latitude(intersectionL.get(i).getConnections().get(j).getLat());
				e.setP2longitude(intersectionL.get(i).getConnections().get(j).getLon());
				e.setDist();
				tmp.add(e);
				deBugEdges.add(e);
				
			}
			if(tmp.size()>1) {
				
				for(int k =1;k <tmp.size();k++) {
					tmp.get(0).connect(tmp.get(k));
				}
			}
			
		}
		
	}
	//parse roads and create road segments (links) only use highways
	public static void importRoads(Document doc) {
		LinkedList<Long> connections = new LinkedList<Long>();
		boolean highway;
		boolean oneway;
		boolean footway;
		boolean track;
		NodeList nList = doc.getElementsByTagName("way");
		int leng = nList.getLength();
		for (int i = 0; i < leng; i++) {
			Node n = nList.item(i);
			Node childNode = n.getFirstChild();
			connections.clear();
			highway = false;
			oneway = false;
			footway = false;
			track = false;
			while( childNode.getNextSibling()!=null ){
				if (childNode.getNodeType() == Node.ELEMENT_NODE && childNode.getNodeName() == "nd") {
					Element childElement = (Element) childNode;
					connections.add(Long.valueOf(childElement.getAttribute("ref")));
				}
				else if (childNode.getNodeType() == Node.ELEMENT_NODE && childNode.getNodeName() == "tag") {
					Element childElement = (Element) childNode;

					if(childElement.getAttribute("k").equals("oneway")) {
						if(childElement.getAttribute("v").equals("yes")) {
							oneway = true;
						}
					}
				}
				childNode = childNode.getNextSibling();
			}
			int klen = connections.size();
			for(int kk = 1; kk < klen; kk++) {
				Intersection a = intersections.get(connections.get(kk-1));
				Intersection b = intersections.get(connections.get(kk));
				//oneway then only one direction link
				if(oneway) {
					Vertex edge = new Vertex(a.getId()+b.getId());
					edge.setP1latitude(a.getLat());
					edge.setP1longitude(a.getLon());
					edge.setP2latitude(b.getLat());
					edge.setP2longitude(b.getLon());
					edges.add(edge);
					a.connect(b);
				}
				//create link in both directions
				else {
					Vertex edge = new Vertex(a.getId()+b.getId());
					edge.setP1latitude(a.getLat());
					edge.setP1longitude(a.getLon());
					edge.setP2latitude(b.getLat());
					edge.setP2longitude(b.getLon());
					edges.add(edge);
					Vertex edge2 = new Vertex((a.getId()+b.getId())*(-1));
					edge2.setP1latitude(b.getLat());
					edge2.setP1longitude(b.getLon());
					edge2.setP2latitude(a.getLat());
					edge2.setP2longitude(a.getLon());
					edges.add(edge2);
					a.connect(b);
					b.connect(a);
				}

			}
				/*for(int k = 0; k < connections.size(); k++) {
					Intersection a = intersections.get(connections.get(k));	
					int startFrom;
					if(!oneway) {
						startFrom = 0;

					}
					else {
						startFrom = k + 1;
					}
					for(int l = startFrom; l < connections.size(); l++) {
						if(k != l) {
							Intersection b = intersections.get(connections.get(l));
			    			a.connect(b);
						}
					}
				}*/

		}
	}


	public static Vertex getVertex(char c) {
		for(Vertex v : edges) {
			if(v.getLabel() == c) {
				return v;
			}
		}
		return null;
	}
	//find closest segment point?
	public static Gps findclosest(Vertex v,Gps g){
		double k = (int) haversine(v.getp1Latitude(),v.getp1Longitude(),v.getp2Latitude(),v.getp2Longitude());
		//double k = 1000;
		Gps result = new Gps(0,0);
		double closest = Double.MAX_VALUE;
		for(int i=1; i<k;i++){
			Gps temp = new Gps(v.getp1Latitude()+(i/k)*(v.getp2Latitude()-v.getp1Latitude()),
					v.getp1Longitude()+(i/k)*(v.getp2Longitude()-v.getp1Longitude()));
			if(distanceBetweenGps(g,temp)<closest){


				closest = distanceBetweenGps(g,temp);		
				result.setLatitude(temp.getLatitude());
				result.setLongitude(temp.getLongitude());
				//System.out.println(result.getLatitude() +" " + result.getLongitude());
			}
		}
		return result;
	}
	//find distance of the road segment
	public static double distance(Vertex v){
		double f = Math.sqrt(Math.pow(v.getp2Latitude()-v.getp1Latitude(),2) +Math.pow(v.getp2Longitude()-v.getp1Longitude(),2));
		return f;
	}
	//find distance of the road segment start point  and projected gps vector
	public static double distanceGps(Vertex v, Gps g){
		double h = Math.sqrt(Math.pow(g.getLatitude()-v.getp1Latitude(),2) +Math.pow(g.getLongitude()-v.getp1Longitude(),2));
		return h;
	}
	//Distance between gps points
	public static double distanceBetweenGps(Gps g0, Gps g){
		double h = Math.sqrt(Math.pow(g.getLatitude()-g0.getLatitude(),2) +Math.pow(g.getLongitude()-g0.getLongitude(),2));
		return h;
	}
	// projection of vector ac on vector ab(line/segment etc)
	public static Gps createProjection(Vertex a,Gps c){
		double[] d = mVector(a); 		
		double[] e = mVector(a,c);
		double f =  (Math.pow(d[0], 2)+Math.pow(d[1], 2));
		double proj = (d[0]*e[0]+d[1]*e[1])/f;
		Gps projVector =new Gps(a.getp1Latitude()+proj*d[0],a.getp1Longitude()+proj*d[1]);		// projVector is the projected point on the road segment	 
		return projVector;	
	}

	//make  the map segment vector
	public static double[] mVector(Vertex a){
		double[] c = {0,0};
		c[0]=a.getp2Latitude()-a.getp1Latitude();
		c[1]=a.getp2Longitude()-a.getp1Longitude();
		return c;
	}
	// vector between start point of map segment and Gps point
	public static double[] mVector(Vertex a, Gps b){
		double[] c = {0,0};
		c[0]=b.getLatitude()-a.getp1Latitude();
		c[1]=b.getLongitude()-a.getp1Longitude();
		return c;
	}

	// vector between 2 gps points
	public static double[] mVector(Gps b, Gps d){
		double[] c = {0,0};
		c[0]=d.getLatitude()-b.getLatitude();
		c[1]=d.getLongitude()-b.getLongitude();
		return c;
	}

	// angle bet<ween 2 vectors a b 
	public static double angelVector(Vertex a, Gps b,Gps c){
		double[] tmp1 = mVector(a);
		double[] tmp2 = mVector(b,c);
		double d = (tmp1[0]*tmp2[0]+tmp1[1]*tmp2[1])/
				(Math.sqrt(Math.pow(tmp1[0],2)+ Math.pow(tmp1[1],2))*
						Math.sqrt(Math.pow(tmp2[0],2)+ Math.pow(tmp2[1],2)));
		return Math.toDegrees(Math.acos(d));
	}

	/**
	 * Calculates the distance in km between two lat/long points
	 * using the haversine formula
	 */
	public static double haversine(
			double lat1, double lng1, double lat2, double lng2) {
		double r = 6372.8; // average radius of the earth in km
		double dLat = Math.toRadians(lat2 - lat1);
		double dLon = Math.toRadians(lng2 - lng1);
		double a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
				Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) 
				* Math.sin(dLon / 2) * Math.sin(dLon / 2);
		double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
		double d = (r * c)*1000;	//end result in meters
		return d;
	}

	//Find first matched link(s)
	public static LinkedList<Vertex> startMapping(ArrayList<Vertex> s,Gps g){
		LinkedList<Vertex> firstPoint = new LinkedList<Vertex>();
		double minDist = Double.MAX_VALUE;
		for(Vertex v1: edges){		
			Gps tmp = createProjection(v1,g);
			if(haversine(v1.getp1Latitude(),v1.getp1Longitude(),g.getLatitude(),g.getLongitude())<minDist){
				minDist = haversine(v1.getp1Latitude(),v1.getp1Longitude(),g.getLatitude(),g.getLongitude());
			}
			if(haversine(tmp.getLatitude(),tmp.getLongitude(),g.getLatitude(),g.getLongitude())<
					15){	
				//System.out.println(tmp.getLatitude()+" "+tmp.getLongitude());
				if(!firstPoint.contains(v1)){
					firstPoint.add(v1);
					//System.out.println(tmp.getLatitude()+" "+tmp.getLongitude());
				}

			}
		}
		return firstPoint;	
	}
	//given a  starting vertex 
	public static void bfs(Vertex v, Gps g0){

		Queue<Vertex> list = new LinkedList<Vertex>();
		LinkedList<Vertex> psG = new LinkedList<Vertex>();
		LinkedList<Vertex> endRe = new LinkedList<Vertex>();
		//HashSet<Vertex> endRe = new HashSet<Vertex>();	//faster but cannot use due to multiple points can be on the same link
		Vertex v1 = null;
		Gps g1 = g0;
		Vertex lastV = v;

		LinkedList<Vertex> startPoints = startMapping(edges, gpses.get(0));	//Starting points
		System.out.println("Found first point");
		g1 = gpses.get(0);

		for(Vertex vv: startPoints){
			psG.add(vv);
		}

		System.out.println("Start finding points");
		//System.out.println(startPoints.size());
		for(int k=1;k<gpses.size();k++){
			Gps g2 = gpses.get(k);
			//list.add(v1);
			boolean check = false;	//check when to end searching
			for(Vertex e: psG){
				list.add(e);
			}
			psG.clear();

			check=false;
			double dist = 0;
			double angle = 0; 
			while(!list.isEmpty()){	
				v1 = list.remove();
				//First checks if the 
				Gps tmp = createProjection(v1,g2);

				tmp = findclosest(v1,tmp);
				
				if(distanceGps(v1,g2)<=distance(v1) && 
						haversine(tmp.getLatitude(),tmp.getLongitude(),g2.getLatitude(),g2.getLongitude())<20 &&
						angelVector(v1,g1,g2)<=90){
					if(!psG.contains(v1)){
						System.out.println(tmp.getLatitude()+" "+tmp.getLongitude());
						//System.out.println(v1.getp1Latitude()+" "+ v1.getp1Longitude());
						//System.out.println(v1.getp2Latitude()+" "+ v1.getp2Longitude());
						dist=distanceBetweenGps(g2,tmp);
						angle=angelVector(v1,g1,g2);
						tmp.setWeight(omegaR*dist+omegaT*angle);
						v1.addGpsPoints(tmp);
						v1.setOriginalG(g2);
						//matchedGps.add(tmp);
						psG.add(v1);	//add the suitable edge to list
						endRe.add(v1);
						v1.setDist();
						//lastV = v1;
						g1 = tmp;
						check = true;

					}
					else{
						psG.add(v1);	
						endRe.add(v1);
					}
				}
				// Compares first by angle then by distance 
				else if(angelVector(v1,g1,g2)<=90){

					//Gps tmp = createProjection(v1,g2);	//Creates the projected point on the map segment
					//distance from last gps point to matching segment endpoint must be less than 2*D between gps points				
					if((v1.getDistWithMe()-distanceGps(v1,g2))+(v.getDistWithMe()-distanceGps(v,g1))<
							2*distanceBetweenGps(g1,g2)){

						if(haversine(tmp.getLatitude(),tmp.getLongitude(),g2.getLatitude(),g2.getLongitude())<20 &&
								distanceGps(v1,g2)<=distance(v1)){
							//tmp = findclosest(v1,tmp);
							//System.out.println("k= "+k+" Found matching position on segment: "+v1.getLabel());
							System.out.println(tmp.getLatitude()+" "+tmp.getLongitude());
							if(!psG.contains(v1)){
								dist=distanceBetweenGps(g2,tmp);
								angle=angelVector(v1,g1,g2);
								tmp.setWeight(omegaR*dist+omegaT*angle);
								v1.addGpsPoints(tmp);
								v1.setOriginalG(g2);
								//matchedGps.add(tmp);
								psG.add(v1);	// add the suitable edge to list
								v1.setDist();
								endRe.add(v1);
								//lastV = v1;
								g1 = tmp;
								check = true;
							}
						}
						else{
							psG.add(v1);	
							endRe.add(v1);
						}
					}
				}
				if(!check){

					//psG.add(v1);
					for(int i =0;i<v1.getConnectionsV().size();i++){
						list.add(v1.getConnectionsV().get(i));
						v1.getConnectionsV().get(i).addDistWithMe(v1.getDistWithMe());
						v1.getConnectionsV().get(i).setPredecessor(v1);		//This should be done when matched gps found too
					}
				}

				//v1.setDist();

			}
			list.clear();
			
		}
		System.out.println();
		//dfs(endRe);
		ArrayList<Vertex> checkAgainst = new ArrayList<Vertex>();
		ArrayList<Vertex> checkE = new ArrayList<Vertex>();
		for(Vertex va : endRe){
			if(va.getGpsPoints().size()!=0){
				matchedGps.add(va.getGpsPoints().get(0));
				checkE.add(va);
				
				//System.out.println(va.getGpsPoints().get(0).getWeight());
			}
		}
		for(Vertex va: checkE) {
			if(!checkAgainst.contains(va)) {
				checkAgainst.add(va);
			}
			else {
				for(Vertex h: checkAgainst) {
					if(h.getOriginalG()==va.getOriginalG()) {
						if(h.getGpsPoints().get(0).getWeight()>va.getGpsPoints().get(0).getWeight()) {
							break;
						}
						else {
							checkAgainst.remove(h);
							checkAgainst.add(va);
							//System.out.println("deld 1");
							break;
						}
					}
				}
			}
					
		}
		matchedGps.clear();
		for(Vertex e: checkAgainst) {
			matchedGps.add(e.getGpsPoints().get(0));
		}
		
	}

	public static LinkedList<Vertex> dfs(LinkedList<Vertex> endRe) {
		ArrayList<ArrayList<Vertex>> paths = new ArrayList<ArrayList<Vertex>>();
		for(int i=endRe.size()-1;i>=0;i--) {
			ArrayList<Vertex> candidatePath = new ArrayList<Vertex>(); 
			int counter = gpses.size();
			Stack<Vertex> stak = new Stack<Vertex>();
			if(endRe.get(i).getGpsPoints().size()>0 && !endRe.get(i).isVisited()) {
				endRe.get(i).setVisited(true);
				stak.push(endRe.get(i));
				//System.out.println(endRe.get(i).getOriginalG().getLatitude()+" "+ endRe.get(i).getOriginalG().getLongitude());
			}
			while(!stak.isEmpty()) {
				//System.out.println(stak.size());
				Vertex v = stak.pop();
				v.setVisited(true);
				if(endRe.contains(v.getPredecessor())) {
					stak.push(v.getPredecessor());
					candidatePath.add(v);
				}

			}
			if(candidatePath.size()>0) {
				paths.add(candidatePath);
			}
		}
		System.out.println();
		for(ArrayList<Vertex> p : paths) {
			for(Vertex v : p) {
				if(v.getGpsPoints().size()>0) {
					System.out.println(v.getOriginalG().getLatitude()+" "+v.getOriginalG().getLongitude());
				}
			}
		}
			
		return endRe;
	}

}
