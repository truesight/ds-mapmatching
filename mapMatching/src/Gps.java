
public class Gps {

	
	private double longitude;
	private double latitude;
	private double weight;
	
	
	public double getLongitude() {
		return longitude;
	}


	public void setLongitude(double d) {
		this.longitude = d;
	}


	public double getLatitude() {
		return latitude;
	}


	public void setLatitude(double d) {
		this.latitude = d;
	}


	public double getWeight() {
		return weight;
	}


	public void setWeight(double weight) {
		this.weight = weight;
	}


	public Gps(double d, double e){
		this.latitude=d;
		this.longitude=e;
		weight =0;
	}
}
