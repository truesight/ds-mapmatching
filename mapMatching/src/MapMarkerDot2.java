


import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;

import org.openstreetmap.gui.jmapviewer.Coordinate;
import org.openstreetmap.gui.jmapviewer.Layer;
import org.openstreetmap.gui.jmapviewer.MapMarkerCircle;
import org.openstreetmap.gui.jmapviewer.Style;


public class MapMarkerDot2 extends MapMarkerCircle {

    public static final int DOT_RADIUS = 5;
    Color color;
    
    public MapMarkerDot2(Coordinate coord) {
        this(null, null, coord);
    }
    public MapMarkerDot2(String name, Coordinate coord) {
        this(null, name, coord);
    }
    public MapMarkerDot2(Layer layer, Coordinate coord) {
        this(layer, null, coord);
    }
    public MapMarkerDot2(Layer layer, String name, Coordinate coord) {
        this(layer, name, coord, getDefaultStyle());
    }
    public MapMarkerDot2(Color color, double lat, double lon) {
        this(null, null, lat, lon);
        setColor(color);
    }
    public MapMarkerDot2(double lat, double lon) {
        this(null, null, lat, lon);
    }
    public MapMarkerDot2(Layer layer, double lat, double lon) {
        this(layer, null, lat, lon);
    }
    public MapMarkerDot2(Layer layer, String name, double lat, double lon) {
        this(layer, name, new Coordinate(lat, lon), getDefaultStyle());
    }
    public MapMarkerDot2(Layer layer, String name, Coordinate coord, Style style) {
        super(layer, name, coord, DOT_RADIUS, STYLE.FIXED, style);
    }

    public static Style getDefaultStyle(){
        return new Style(Color.BLACK, Color.RED, null, getDefaultFont());
    }
    
    public void paint(Graphics g, Point position) {
        int size_h = 5;
        int size = size_h * 2;
        g.setColor(color);
        g.fillOval(position.x - size_h, position.y - size_h, size, size);
        g.setColor(Color.BLACK);
        g.drawOval(position.x - size_h, position.y - size_h, size, size);
    }

        Color indicationColor = Color.green;

        public void paintIndication(Graphics g, Point position) {
                int size_h = 10;
                int size = size_h * 2;

                g.setColor(this.indicationColor);
                g.fillOval(position.x - size_h, position.y - size_h, size, size);
        }
}
