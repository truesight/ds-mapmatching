
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;


public class ShortestPath {
	static Map<Long, Intersection> intersections;
	static ArrayList<Intersection> intersectionList;
	static ArrayList<Vertex> edges;

	public static void main(String[] args) throws Exception {
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbf.newDocumentBuilder(); 
		Document doc = db.parse(new File("map.osm"));

		intersections = new HashMap<Long, Intersection>();
		intersectionList = new ArrayList<Intersection>();
		edges = new ArrayList<Vertex>();
		
		importIntersections(doc);
		importRoads(doc);
		intersectionPruning();
		nodeToEdge(edges);
		System.out.println(edges.size()+" "+ intersections.size());
		
	}

	public static void importIntersections(Document doc) throws IOException {
		NodeList nList = doc.getElementsByTagName("node");
		for (int i = 0; i < nList.getLength(); i++) {
			Node n = nList.item(i);
			Element e = (Element) n;
			long id = Long.valueOf(e.getAttribute("id"));
			Intersection inter = new Intersection(id);
			inter.setLatLon(Double.valueOf(e.getAttribute("lat")), Double.valueOf(e.getAttribute("lon")));
			intersections.put(id, inter);
		}
	}
	
	public static void nodeToEdge(ArrayList<Vertex> e){
		for(int k=0;k<edges.size();k++){
			for(int l=0;l<edges.size();l++){
				if(edges.get(k).getp2Latitude()== edges.get(l).getp1Latitude() &&
						edges.get(k).getp2Longitude()== edges.get(l).getp1Longitude()){
					edges.get(k).connect(edges.get(l));
				}
			}
		}
		
	}
	
	public static void importRoads(Document doc) {
		LinkedList<Long> connections = new LinkedList<Long>();
		boolean highway;
		boolean oneway;
		boolean footway;
		boolean track;
		NodeList nList = doc.getElementsByTagName("way");
		for (int i = 0; i < nList.getLength(); i++) {
			Node n = nList.item(i);
			Node childNode = n.getFirstChild();
			connections.clear();
			highway = false;
			oneway = false;
			footway = false;
			track = false;
		    while( childNode.getNextSibling()!=null ){
		        if (childNode.getNodeType() == Node.ELEMENT_NODE && childNode.getNodeName() == "nd") {
		            Element childElement = (Element) childNode;
		            connections.add(Long.valueOf(childElement.getAttribute("ref")));
		        }
		        else if (childNode.getNodeType() == Node.ELEMENT_NODE && childNode.getNodeName() == "tag") {
		        	Element childElement = (Element) childNode;
		        	if(childElement.getAttribute("k").equals("highway")) {
		        		highway = true;
		        		if(childElement.getAttribute("v").equals("footway")) {
			        		footway = true;
			        	}
			        	else if(childElement.getAttribute("v").equals("track")) {
			        		track = true;
			        	}
		        	}
		        	else if(childElement.getAttribute("k").equals("oneway")) {
		        		oneway = true;
		        	}
		        }
		        childNode = childNode.getNextSibling();
		    }
		    if(highway && !footway && !track) {
		    	
		    	for(int kk = 1; kk < connections.size(); kk++) {
			    	Intersection a = intersections.get(connections.get(kk-1));
			    	Intersection b = intersections.get(connections.get(kk));
			    	
			    	if(oneway) {
			    		Vertex edge = new Vertex(a.getId()+b.getId());
			    		edge.setP1latitude(a.getLat());
			    		edge.setP1longitude(a.getLon());
			    		edge.setP2latitude(b.getLat());
			    		edge.setP2longitude(b.getLon());
			    		edges.add(edge);
	    			}
			    	else {
			    		Vertex edge = new Vertex(a.getId()+b.getId());
			    		edge.setP1latitude(a.getLat());
			    		edge.setP1longitude(a.getLon());
			    		edge.setP2latitude(b.getLat());
			    		edge.setP2longitude(b.getLon());
			    		edges.add(edge);
			    		Vertex edge2 = new Vertex((a.getId()+b.getId())*(-1));
			    		edge2.setP1latitude(b.getLat());
			    		edge2.setP1longitude(b.getLon());
			    		edge2.setP2latitude(a.getLat());
			    		edge2.setP2longitude(a.getLon());
			    		edges.add(edge2);
			    	}
			    	
			    }
		    	for(int k = 0; k < connections.size(); k++) {
			    	Intersection a = intersections.get(connections.get(k));
			    	
			    	int startFrom;
			    	if(!oneway) {
			    		startFrom = 0;
			    		
	    			}
			    	else {
			    		startFrom = k + 1;
			    	}
			    	for(int l = startFrom; l < connections.size(); l++) {
			    		if(k != l) {
			    			Intersection b = intersections.get(connections.get(l));
			    			a.connect(b);
			    			
			    		}
			    	}
			    }
		    }
		}
	}
	
	public static void intersectionPruning() {
		Iterator<Entry<Long, Intersection>> it = intersections.entrySet().iterator();
	    while (it.hasNext()) {
	        Map.Entry<Long, Intersection> pairs = (Entry<Long, Intersection>)it.next();
	        if(pairs.getValue().getConnections().size() == 0) {
	        	it.remove(); // avoids a ConcurrentModificationException
	        }
	        else {
	        	intersectionList.add(pairs.getValue());
	        }
	    }
	}
	
	public static void aStar(Intersection start, Intersection end) {
		ArrayList<Intersection> closedList = new ArrayList<Intersection>();
		ArrayList<Intersection> openList = new ArrayList<Intersection>();
        Map<Intersection, Intersection> cameFrom = new HashMap<Intersection, Intersection>();
        
        openList.add(start);
        start.setGscore(0);
        start.setFscore(start.getGscore() + start.distanceFrom(end));
        
        Intersection current;
        double tentative_g_score;
        while(!openList.isEmpty()) {
        	Collections.sort(openList);
        	current = openList.get(0);
        	if(current == end) {
        		reconstructPath(current, start, cameFrom);
        		return;
        	}
        	openList.remove(current);
        	closedList.add(current);
        	for(Intersection i : current.getConnections()) {
        		if(!(closedList.contains(i))) {
        			tentative_g_score = current.getGscore() + current.distanceFrom(i);
        			if(tentative_g_score < i.getGscore() || !(openList.contains(i))) {
        				cameFrom.put(i, current);
        				i.setGscore(tentative_g_score);
        				i.setFscore(i.getGscore() + i.distanceFrom(end));
        				if(!(openList.contains(i))) {
        					openList.add(i);
        				}
        			}
        		}
        		//System.out.println(i.getGscore() + " " + i.getFscore());
        	}
        }
        System.out.println("Failure");
	}
	
	public static void reconstructPath(Intersection is, Intersection start, Map<Intersection, Intersection> cameFrom) {
		LinkedList<Intersection> path = new LinkedList<Intersection>();
		path.add(is);
		Intersection current = is;
		System.out.println("Path:");
		System.out.print("www.google.ee/maps/dir");
		while(current != start) {
			current = cameFrom.get(current);
			path.add(current);
		}
		for(int i = 0; i < path.size(); i++) {
			//System.out.print(path.get(path.size()-i-1).getId() + " ");
			System.out.print("/" + path.get(path.size()-i-1).getLat() + "+" + path.get(path.size()-i-1).getLon());
		}
		System.out.print("/");
	}

}
